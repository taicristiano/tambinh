<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 Template name: Wedding-template
 
 * @package fabframe
 */
 
get_header(); ?>

	<div class="wedbox">
		<div class="bg-name"> <?php echo ft_of_get_option('fabthemes_groom'); ?> <span>weds</span> <?php echo ft_of_get_option('fabthemes_bride'); ?> </div>
		<div class="wed-date"> <span><?php echo ft_of_get_option('fabthemes_date'); ?></span> </div>
	</div>
<div id="hblock">

<div id="home-slider">


<ul class="slides-container">
<?php

$slidecount = ft_of_get_option('fabthemes_slidecount','5');
$args = array( 'posts_per_page' => $slidecount, 'post_type'=> 'slide' );

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<li>
		    <?php
			$thumb = get_post_thumbnail_id();
			$img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
			?>
		   <img src="<?php echo $img_url; ?>" />
	
	</li>
<?php endforeach; 
wp_reset_postdata();?>

</ul>


    <nav class="slides-navigation">
	    <a href="#" class="next glyphicon glyphicon-chevron-right"></a>
	    <a href="#" class="prev glyphicon glyphicon-chevron-left"></a>
	</nav>
</div>
</div>
<?php get_footer(); ?>